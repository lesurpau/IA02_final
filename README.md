### Member
```prolog
member(X, [X|_]).
member(X, [_|Tail]) :-,
  member(X, Tail).
```

### Append / Concat
```prolog
append([],X,X).
append([X|Y],Z,[X|W]) :- append(Y,Z,W).
```

### Reverse
```prolog
reverse([],[]).
reverse([X|Xs],YsX) :- reverse(Xs,Ys), append(Ys,[X],YsX).
```

### Length
```prolog
len([], 0).
len([_ | Tail], Length) :-
    len(Tail, Length1),
    Length is Length1 + 1,!.
```

### Nième élément
```prolog
element_n(1,[X|Q],X):-!.
element_n(N,[T|Q],X):-M is N-1, element_n(M,Q,X).
```

### Tri à bulles
```prolog
bubble_sort(List,Sorted):-b_sort(List,[],Sorted).
b_sort([],Acc,Acc).
b_sort([H|T],Acc,Sorted):-bubble(H,T,NT,Max),b_sort(NT,[Max|Acc],Sorted).

bubble(X,[],[],X).
bubble(X,[Y|T],[Y|NT],Max):-X>Y,bubble(X,T,NT,Max).
bubble(X,[Y|T],[X|NT],Max):-X=<Y,bubble(Y,T,NT,Max).
```
### flatten
```prolog
flatten([[X|S]|T], F) :- flatten([X|[S|T]], F).
flatten([[]|S], F) :- flatten(S, F).
flatten([X|S], [X|T]) :- \+(X = []), \+(X = [_|_]), flatten(S, T).
flatten([], []).
```
### enleve
```prolog
retire(_,[],[]).
retire(T, [T|Q], Q).
retire(X, [T|Q], [T|NewListe]):-retire(X, Q, NewListe).
```
### ajoute
```prolog
ajoute(X,[],[X]).
ajoute(X,L,L2):-concat(L,[X],L2).
```
